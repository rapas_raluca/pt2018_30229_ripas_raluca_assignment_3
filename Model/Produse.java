package Model;
/**
 * Clasa Produse reprezinta produsul
 * @author      Ripas Raluca
 */
public class Produse {
    private int idProdus;
    private String nume;
    private double pret;
    private int stoc;

    public Produse(int idProdus,String nume,double pret,int stoc){
        this.idProdus=idProdus;
        this.nume=nume;
        this.pret=pret;
        this.stoc=stoc;
    }

    public Produse(){

    }

    public int getIdProdus() {
        return idProdus;
    }

    public String getNume() {
        return nume;
    }

    public double getPret() {
        return pret;
    }

    public int getStoc() {
        return stoc;
    }

    public void setIdProdus(int idProdus){
        this.idProdus=idProdus;
    }

    public void setNume(String nume){
        this.nume=nume;
    }

    public void setPret(double pret){
        this.pret=pret;
    }

    public void setStoc(int stoc){
        this.stoc=stoc;
    }

    @Override
    public String toString() {
        return "Produs [id="+idProdus+ " nume="+nume+ " pret="+pret+" stoc="+stoc;
    }
    public String toString2() {
        return "Produs: "+idProdus+ "    Nume: "+nume+ "    Pret/Buc: "+pret+"    Cantitate: ";
    }
}
