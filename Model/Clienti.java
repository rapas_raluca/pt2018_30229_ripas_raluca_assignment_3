package Model;
// import statements

/**
 * Clasa Clienti reprezinta clientul
 * @author      Ripas Raluca
 */

public class Clienti {
    private int idClient;
    private String nume;
    private String prenume;
    private String adresa;
    private String telefon;
    private String email;
    public Clienti(int idClient,String nume,String prenume,String adresa,String telefon, String email){
        this.idClient=idClient;
        this.nume=nume;
        this.prenume=prenume;
        this.adresa=adresa;
        this.telefon=telefon;
        this.email=email;
    }

    public Clienti(){
    }

    public int getIdClient() {
        return idClient;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setIdClient(int idClient){
        this.idClient=idClient;
    }

    public void setNume(String nume){
        this.nume=nume;
    }

    public void setPrenume(String prenume){
        this.prenume=prenume;
    }

    public void setAdresa(String adresa){
        this.adresa=adresa;
    }

    public void setTelefon(String telefon){
        this.telefon=telefon;
    }
    public  void setEmail(String email){
        this.email=email;
    }

    public String toString(){
        return "Client: "+idClient+"\r\nNume: "+nume+ " "+prenume+"\r\nAdresa: "+adresa+"\r\nTelefon: "+telefon+ "\r\nEmail:"+ email;
    }

}
