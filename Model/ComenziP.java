package Model;
/**
 * Clasa ComenziP retine datele despre comanda si un produs
 * @author      Ripas Raluca
 */
public class ComenziP {
    private int idComanda;
    private int idProdus;
    private int cantitate;
    private double total;

    public ComenziP(int idComanda,int idProdus,int cantitate,double total){
        this.idComanda=idComanda;
        this.idProdus=idProdus;
        this.cantitate=cantitate;
        this.total=total;
    }

    public ComenziP(){

    }
    public int getIdComanda() {
        return idComanda;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public int getCantitate() {
        return cantitate;
    }

    public double getTotal() {
        return total;
    }

    public void setIdComanda(int idComanda) {
        this.idComanda = idComanda;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ComandaP [id="+ idComanda+" idProdus="+idProdus+" cantitate="+ cantitate+" total="+total;
    }
}
