package Model;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
/**
 * Clasa ComenziC reprezinta comanda si datele utile desprea aceasta
 * @author      Ripas Raluca
 */
public class ComenziC {
    private int idComanda;
    private int idClient;
    private String metodaPlata;
    private Date dataComanda;
    private double totalPlata;

    public ComenziC(int idComanda,int idClient,Date dataComanda,String metodaPlata,double totalPlata){
        this.idComanda=idComanda;
        this.idClient=idClient;
        this.dataComanda=dataComanda;
        this.metodaPlata=metodaPlata;
        this.totalPlata=totalPlata;
    }
    public ComenziC(){

    }
    public int getIdComanda() {
        return idComanda;
    }

    public int getIdClient() {
        return idClient;
    }

    public Date getDataComanda() {
        return dataComanda;
    }

    public double getTotalPlata() {
        return totalPlata;
    }

    public String getMetodaPlata() {
        return metodaPlata;
    }

    public void setDataComanda(Date dataComanda) {
        this.dataComanda = dataComanda;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public void setIdComanda(int idComanda) {
        this.idComanda = idComanda;
    }

    public void setMetodaPlata(String metodaPlata) {
        this.metodaPlata = metodaPlata;
    }

    public void setTotalPlata(double totalPlata) {
        this.totalPlata = totalPlata;
    }

    @Override
    public String toString() {
        return "ComandaC [id="+idComanda+ " idClient="+idClient+" metodaPlata="+metodaPlata+ " dataComanda="+dataComanda+" totalPlata="+totalPlata;
    }
}
