package Presentation;

/**
 * Clasa Controller face legatura intre Business Logic si View(interfata)
 * @author      Ripas Raluca
 */
import Model.Clienti;
import Model.ComenziC;
import Model.ComenziP;
import Model.Produse;
import bll.ClientiBll;
import bll.ComenziCBll;
import bll.ComenziPBll;
import bll.ProduseBll;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.format.DateTimeFormatter.ofPattern;

public class Controller {

    private View v;
    ProduseBll plogic= new ProduseBll();
    ClientiBll clogic= new ClientiBll();
    ComenziCBll cclogic= new ComenziCBll();
    ComenziPBll cplogic= new ComenziPBll();
    int nrProdus;
    int cantitate;
    int ok=1;
    int idClient=0;
    int idComanda=0;
    String metoda="";
    java.sql.Date sqlStartDate=null;
    ArrayList<Produse> listaProduse=new ArrayList<>();
    ArrayList<Integer> listaCantitati= new ArrayList<>();
    public Controller(View v, ProduseBll plogic, ClientiBll clogic,ComenziCBll cclogic, ComenziPBll cplogic){
            this.v=v;
            this.plogic=plogic;
            this.clogic=clogic;
            this.cclogic=cclogic;
            this.cplogic=cplogic;
            v.adminButtonListener(new adminButtonListener());
            v.clientAdminButtonListener(new clientAdminButtonListener());
            v.displayPAdminButtonListener(new displayPAdminButtonListener());
            v.insertPButtonListener(new insertPButtonListener());
            v.deletePButtonListener(new deletePButtonListener());
            v.updatePButtonListener(new updatePButtonListener());
            v.displayCPAdminButtonListener(new displayCAdminButtonListener());
            v.insertCButtonListener(new insertCButtonListener());
            v.deleteCButtonListener(new deleteCButtonListener());
            v.updateCButtonListener(new updateCButtonListener());
            v.displayDataAdminButtonListener(new displayDataAdminButtonListener());
            v.insertComCButtonListener(new insertComCButtonListener());
            v.deleteComCButtonListener(new deleteComCButtonListener());
            v.updateComCButtonListener(new updateComCButtonListener());
            v.insertComPButtonListener(new insertComPButtonListener());
            v.deleteComPButtonListener(new deleteComPButtonListener());
            v.updateComPButtonListener(new updateComPButtonListener());
            v.printComandaButtonListener(new printComandaButtonListener());

    }

    private class adminButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                v.openAdminFrame();
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class clientAdminButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                v.openClientFrame(clogic.getTable(),plogic.getTable(),cclogic.getTable());
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }



    private class displayPAdminButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                v.setTable(plogic.getTable(),1);
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class insertPButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Produse p= new Produse(Integer.parseInt(v.getIdProdusText()),v.getNumePText(),Double.parseDouble(v.getPretPText()),Integer.parseInt(v.getStocPText()));
                plogic.insertProdus(p);
                v.setTable(plogic.getTable(),1);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class deletePButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Produse p= new Produse(Integer.parseInt(v.getIdProdusText()),v.getNumePText(),Double.parseDouble(v.getPretPText()),Integer.parseInt(v.getStocPText()));
                plogic.deleteProdus(p);
                v.setTable(plogic.getTable(),1);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class updatePButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Produse p= new Produse(Integer.parseInt(v.getIdProdusText()),v.getNumePText(),Double.parseDouble(v.getPretPText()),Integer.parseInt(v.getStocPText()));
                plogic.updateProdus(p);
                v.setTable(plogic.getTable(),1);

                Produse p1= new Produse();

                for(Produse pr: plogic.getList()){
                    if(pr.getIdProdus()==Integer.parseInt(v.getIdProdusText()))
                        p1=pr;
                }
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    private class displayCAdminButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                v.setTable(clogic.getTable(),0);
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class insertCButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Clienti c= new Clienti(Integer.parseInt(v.getIdClientiText()),v.getNumeCText(),v.getPrenumeCText(),v.getAdresaCText(),v.getTelefonCText(),v.getEmailCText());
                clogic.insertClient(c);
                v.setTable(clogic.getTable(),0);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class deleteCButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Clienti c= new Clienti(Integer.parseInt(v.getIdClientiText()),v.getNumeCText(),v.getPrenumeCText(),v.getAdresaCText(),v.getTelefonCText(),v.getEmailCText());
                clogic.deleteClient(c);
                v.setTable(clogic.getTable(),0);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class updateCButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Clienti c= new Clienti(Integer.parseInt(v.getIdClientiText()),v.getNumeCText(),v.getPrenumeCText(),v.getAdresaCText(),v.getTelefonCText(),v.getEmailCText());
                clogic.updateClient(c);
                v.setTable(clogic.getTable(),0);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class displayDataAdminButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                v.setTable(cclogic.getTable(),2);
                v.setTable2(cplogic.getTable());
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class insertComCButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
               SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                java.util.Date date = null;
                try {
                    date = sdf1.parse(v.getDataComandaText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
                ComenziC c=new ComenziC(Integer.parseInt(v.getIdComandaText()),Integer.parseInt(v.getIdClientLText()),sqlStartDate,
                        v.getMetodaPlataTexr(),Double.parseDouble(v.getTotalPlataText()));
                cclogic.insertComandaC(c);
                v.setTable(cclogic.getTable(),2);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class deleteComCButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                java.util.Date date = null;
                try {
                    date = sdf1.parse("20-02-2018");
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
                ComenziC c=new ComenziC(Integer.parseInt(v.getIdComandaText()),Integer.parseInt(v.getIdClientLText()),sqlStartDate,
                        "",0);
                cclogic.deleteComandaC(c);
                v.setTable(cclogic.getTable(),2);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class updateComCButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                java.util.Date date = null;
                try {
                    date = sdf1.parse(v.getDataComandaText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
                ComenziC c=new ComenziC(Integer.parseInt(v.getIdComandaText()),Integer.parseInt(v.getIdClientLText()),sqlStartDate,
                        v.getMetodaPlataTexr(),Double.parseDouble(v.getTotalPlataText()));
                cclogic.updateComandaC(c);
                v.setTable(cclogic.getTable(),2);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class insertComPButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                ComenziP c= new ComenziP(Integer.parseInt(v.getIdComanda2Text()),Integer.parseInt(v.getIdProdusLText()),Integer.parseInt(v.getCantitateText()),
                        Double.parseDouble(v.getTotalText()));
                cplogic.insertComandaP(c);
                v.setTable2(cplogic.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class deleteComPButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                ComenziP c= new ComenziP(Integer.parseInt(v.getIdComanda2Text()),Integer.parseInt(v.getIdProdusLText()),0,0);
                cplogic.deleteComandaP(c);
                v.setTable2(cplogic.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class updateComPButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                ComenziP c= new ComenziP(Integer.parseInt(v.getIdComanda2Text()),Integer.parseInt(v.getIdProdusLText()),Integer.parseInt(v.getCantitateText()),
                        Double.parseDouble(v.getTotalText()));
                cplogic.updateComandaP(c);
                v.setTable2(cplogic.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class printComandaButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String polPatt = "(\\d*,)|(\\d*)";
            Pattern pat = Pattern.compile(polPatt);
            Matcher m = pat.matcher(v.getComandaIdProduseText());
            Matcher m2= pat.matcher(v.getComandaNrProduseText());
            ok=1;
            metoda=v.getComandaMetodaText();
            try {
                sqlStartDate = getComandaDate(v.getComandaDataText());
                idClient = Integer.parseInt(v.getComandaIdClientText());
                idComanda = Integer.parseInt(v.getComandaIdComandaText());
            } catch (NullPointerException ex) {
                System.out.println(ex.getMessage());
                ok=0;
            }
            Produse p=new Produse();
            if(cclogic.findComandaCById(idComanda)==null) {
                while (m.find() && m2.find()) {
                    getProduse(m);
                    getCantitate(m2);
                    p = plogic.findProdusById(nrProdus);
                    if (p.getStoc() >= cantitate) {
                        listaProduse.add(p);
                        listaCantitati.add(cantitate);
                        plogic.updateProdus(p);
                    } else {
                        ok = 0;
                        JOptionPane.showMessageDialog(null, "Stoc insuficient pentru produsul " + nrProdus);
                        System.out.println("Stoc insuficient pentru produsul " + nrProdus);
                    }
                    System.out.println(p.toString());
                }
                listaProduse.remove(p);
                insertComenzi();
                v.setTableProduse(plogic.getTable());
                print();
            }
            else{
                JOptionPane.showMessageDialog(null, "Comanda cu id-ul " + idComanda+ "exista deja");
            }
        }

        private void insertComenzi(){
            if(ok==1){
                double totalPlata=getTotalPlata(listaProduse,listaCantitati);
                ComenziC comanC= new ComenziC(idComanda,idClient,sqlStartDate,metoda,totalPlata);
                cclogic.insertComandaC(comanC);
                v.setTableComenzi(cclogic.getTable());
                for(int i=0;i<listaProduse.size();i++){
                    Produse pr=listaProduse.get(i);
                    int  cant=listaCantitati.get(i);
                    pr.setStoc(pr.getStoc()-cant);
                    plogic.updateProdus(pr);
                    ComenziP comanP=new ComenziP(idComanda,pr.getIdProdus(),cant,cant*pr.getPret());
                    cplogic.insertComandaP(comanP);
                    System.out.println(comanP);
                }
            }
        }

        private void print(){
            try {
                FileWriter fw = new FileWriter(idComanda+".txt", false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter finalOut = new PrintWriter(bw);
                finalOut.println("                                      FACTURA       \n\n");
                finalOut.println("");
                finalOut.println("     CLIENT ");
                finalOut.print(clogic.findClientById(idClient).toString());
                finalOut.println("Metoda Plata: "+metoda);
                finalOut.println("Data: "+sqlStartDate);
                finalOut.println("\n");
                finalOut.println("     PRODUSE  ");
                for(int i=0;i<listaProduse.size();i++){
                    Produse pr=listaProduse.get(i);
                    int  cant=listaCantitati.get(i);
                    finalOut.println(pr.toString2()+cant+"    Pret: "+pr.getPret()*cant);
                }
                finalOut.println( "\n                                                                       TOTAL PLATA: "+getTotalPlata(listaProduse,listaCantitati));
                finalOut.println("\n");
                finalOut.close();
                fw.close();
                bw.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        private double getTotalPlata(ArrayList<Produse> listaProduse, ArrayList<Integer> listaCantitati){
            double totalPlata=0;
            for(int i=0;i<listaProduse.size();i++){
                Produse pr=listaProduse.get(i);
                int  cant=listaCantitati.get(i);
                totalPlata=totalPlata+cant*pr.getPret();
                System.out.println("TOTAAAAL "+totalPlata);
            }
            return totalPlata;
        }
        private void getCantitate(Matcher m){
            try{

                if(m.group(1)!=null ){
                    String c="";
                    int i=0;
                    while(m.group(1).charAt(i)!=','){
                        c=c+m.group(1).charAt(i);
                        i++;
                    }
                    cantitate=Integer.parseInt(c);
                }
                else{
                    if(m.group(2)!=null ){
                        cantitate=Integer.parseInt(m.group(2));
                    }
                }
            } catch (NullPointerException | NumberFormatException ex) {
                System.out.println(ex.getMessage());
            }
        }

        private void getProduse(Matcher m){
            try{

                    if(m.group(1)!=null ){
                        String c="";
                        int i=0;
                        while(m.group(1).charAt(i)!=','){
                            c=c+m.group(1).charAt(i);
                            i++;
                        }
                        nrProdus=Integer.parseInt(c);
                    }
                    else{
                        if(m.group(2)!=null ){
                            nrProdus=Integer.parseInt(m.group(2));
                        }
                    }
            } catch (NullPointerException | NumberFormatException ex) {
                System.out.println(ex.getMessage());
            }
        }

        private Date getComandaDate(String d){


                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                java.util.Date date = null;
                try {
                    date = sdf1.parse(d);

                } catch (ParseException ex) {
                    System.out.println(ex.getMessage());
                }
                java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
                return sqlStartDate;

        }
    }
}
