package Presentation;

import Model.Clienti;
import Model.Produse;
import bll.ClientiBll;
import bll.ProduseBll;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

public class View extends JFrame{
    private  JFrame frame= new JFrame();
    private  JFrame frameAdmin;
    private  JFrame frameClient= new JFrame();
    private JTabbedPane tabbedPane=new JTabbedPane();
    private JPanel panelClienti= new JPanel();
    private JPanel panel= new JPanel();
    private JPanel panelProduse = new JPanel();
    private JPanel panelComenzi = new JPanel();

    private JButton admin=new JButton("Admin");
    private JButton clientAdmin=new JButton("Client");
    private JTable table ;

    private JScrollPane scrtable= new JScrollPane();
    private JScrollPane scrtable2= new JScrollPane();
    private JScrollPane scrtable3= new JScrollPane();
    private JScrollPane scrtable4= new JScrollPane();
    private JScrollPane scrtable5= new JScrollPane();
    private JLabel idProdus= new JLabel("IDProdus:");
    private JLabel numeP= new JLabel("Nume:");
    private JLabel pretP= new JLabel("Pret:");
    private JLabel stocP = new JLabel("Stoc:");

    private JTextField idProdusText=new JTextField();
    private JTextField numePText= new JTextField();
    private JTextField pretPText= new JTextField();
    private JTextField stocPText= new JTextField();

    private JButton displayP= new JButton("Display Data");
    private JButton insertP= new JButton("INSERT");
    private JButton deleteP= new JButton("DELETE");
    private JButton updateP= new JButton("UPDATE");

    private JLabel idClient = new JLabel("idClient:");
    private JLabel numeC= new JLabel("Nume:");
    private JLabel prenumeC= new JLabel("Prenume:");
    private JLabel adresaC= new JLabel("Adresa:");
    private JLabel telefonC= new JLabel("Telefon:");
    private JLabel emailC= new JLabel("Email:");
    private JTextField idClientText = new JTextField();
    private JTextField numeCText = new JTextField();
    private JTextField prenumeCText= new JTextField();
    private JTextField adresaCText= new JTextField();
    private JTextField telefonCText= new JTextField();
    private JTextField emailCText=new JTextField();

    private JButton displayC= new JButton("Display Data");
    private JButton insertC= new JButton("INSERT");
    private JButton deleteC= new JButton("DELETE");
    private JButton updateC= new JButton("UPDATE");

    private JLabel idComanda= new JLabel("IdComanda:");
    private JLabel idComanda2= new JLabel("IdComanda:");
    private JLabel idClientL= new JLabel("IdClient:");
    private JLabel metodaPlata= new JLabel("Metoda de plata:");
    private JLabel dataComanda= new JLabel("Data comenzii:");
    private JLabel totalPlata= new JLabel("Total plata:");
    private JLabel idProdusL= new JLabel("IdProdus:");
    private JLabel cantitate= new JLabel("Cantitate:");
    private JLabel total= new JLabel("Total produs:");

    private JTextField idComandaText= new JTextField();
    private JTextField idComandaText2= new JTextField();
    private JTextField idClientLText= new JTextField();
    private JTextField metodaPlataText= new JTextField();
    private JTextField dataComandaText= new JTextField();
    private JTextField totalPlataText=new JTextField();
    private JTextField idProdusLText= new JTextField();
    private JTextField cantitateText= new JTextField();
    private JTextField totalText = new JTextField();

    private JButton displayData= new JButton("Display Data");
    private JButton deleteComC= new JButton("DELETE");
    private JButton insertComC= new JButton("INSERT");
    private JButton updateComC= new JButton("UPDATE");
    private JButton deleteComP= new JButton("DELETE");
    private JButton insertComP= new JButton("INSERT");
    private JButton updateComP= new JButton("UPDATE");

    private JLabel comandaIdClient= new JLabel("Id Client:");
    private JLabel comandaIdComanda=new JLabel("Id Comanda:");
    private JLabel comandaIdProduse=new JLabel("Id Produse (ex: 2,6,3):");
    private JLabel comandaNrProduse= new JLabel("Cantitate (ex: 3,9,10):");
    private JLabel comandaMetoda=new JLabel("Metoda plata:");
    private JLabel comandaData= new JLabel("Data:");

    private JTextField comandaIdClientText= new JTextField();
    private JTextField comandaIdComandaText= new JTextField();
    private JTextField comandaIdProduseText= new JTextField();
    private JTextField comandaNrProduseText= new JTextField();
    private JTextField comandaMetodaText= new JTextField();
    private JTextField comandaDataText= new JTextField();

    private JButton printComanda=new JButton("Plaseaza comanda");
    public View() {
        frame.setTitle("WareHouse");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 250);
        frame.setLocation(350, 100);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setBackground(Color.decode("#FAEBD7"));
        admin.setBounds(55,70,70,50);
        frame.getContentPane().add(admin);
        clientAdmin.setBounds(170,70,70,50);
        frame.getContentPane().add(clientAdmin);

        frame.setVisible(true);
    }

    public void openAdminFrame(){
        frameAdmin=new JFrame();
        frameAdmin.setTitle("Admin");
        frameAdmin.setSize(800, 600);
        frameAdmin.setLocation(350, 100);
        frameAdmin.getContentPane().setLayout(null);
        frameAdmin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        panelProduse.setLayout(null);
        panelClienti.setLayout(null);
        panelComenzi.setLayout(null);

        addOnPanelProduse();
        addOnClientiPanel();
        addOnPanelComenzi();
        tabbedPane.addTab("Clienti",panelClienti);
        tabbedPane.addTab("Produse",panelProduse);
        tabbedPane.addTab("Comenzi",panelComenzi);
        frameAdmin.setContentPane(tabbedPane);

        frameAdmin.setVisible(true);

    }

    private void addOnClientiPanel(){
        idClient.setBounds(10,10,100,20);
        panelClienti.add(idClient);
        idClientText.setBounds(100,10,100,20);
        panelClienti.add(idClientText);
        numeC.setBounds(10,40,100,20);
        panelClienti.add(numeC);
        numeCText.setBounds(100,40,100,20);
        panelClienti.add(numeCText);
        prenumeC.setBounds(10,70,100,20);
        panelClienti.add(prenumeC);
        prenumeCText.setBounds(100,70,100,20);
        panelClienti.add(prenumeCText);
        adresaC.setBounds(10,100,100,20);
        panelClienti.add(adresaC);
        adresaCText.setBounds(100,100,100,20);
        panelClienti.add(adresaCText);
        telefonC.setBounds(10,130,100,20);
        panelClienti.add(telefonC);
        telefonCText.setBounds(100,130,100,20);
        panelClienti.add(telefonCText);
        emailC.setBounds(10,160,100,20);
        panelClienti.add(emailC);
        emailCText.setBounds(100,160,100,20);
        panelClienti.add(emailCText);
        displayC.setBounds(500,20,150,20);
        panelClienti.add(displayC);

        insertC.setBounds(250,70,100,20);
        panelClienti.add(insertC);

        deleteC.setBounds(400,70,100,20);
        panelClienti.add(deleteC);

        updateC.setBounds(550,70,100,20);
        panelClienti.add(updateC);

    }
    private void addOnPanelProduse(){
        idProdus.setBounds(10,10,100,20);
        panelProduse.add(idProdus);
        idProdusText.setBounds(100,10,100,20);
        panelProduse.add(idProdusText);
        numeP.setBounds(10,40,100,20);
        panelProduse.add(numeP);
        numePText.setBounds(100,40,100,20);
        panelProduse.add(numePText);
        pretP.setBounds(10,70,100,20);
        panelProduse.add(pretP);
        pretPText.setBounds(100,70,100,20);
        panelProduse.add(pretPText);
        stocP.setBounds(10,100,100,20);
        panelProduse.add(stocP);
        stocPText.setBounds(100,100,100,20);
        panelProduse.add(stocPText);

        displayP.setBounds(500,20,150,20);
        panelProduse.add(displayP);

        insertP.setBounds(250,70,100,20);
        panelProduse.add(insertP);

        deleteP.setBounds(400,70,100,20);
        panelProduse.add(deleteP);

        updateP.setBounds(550,70,100,20);
        panelProduse.add(updateP);

    }

     private void addOnPanelComenzi(){
        idComanda.setBounds(10,10,100,20);
        panelComenzi.add(idComanda);
        idComandaText.setBounds(110,10,100,20);
        panelComenzi.add(idComandaText);
        idClientL.setBounds(10,30,100,20);
        panelComenzi.add(idClientL);
        idClientLText.setBounds(110,30,100,20);
        panelComenzi.add(idClientLText);
        metodaPlata.setBounds(10,50,100,20);
        panelComenzi.add(metodaPlata);
        metodaPlataText.setBounds(110,50,100,20);
        panelComenzi.add(metodaPlataText);
        dataComanda.setBounds(10,70,100,20);
        panelComenzi.add(dataComanda);
        dataComandaText.setBounds(110,70,100,20);
        panelComenzi.add(dataComandaText);
        totalPlata.setBounds(10,90,100,20);
        panelComenzi.add(totalPlata);
        totalPlataText.setBounds(110,90,100,20);
        panelComenzi.add(totalPlataText);

        insertComC.setBounds(10,140,80,18);
        panelComenzi.add(insertComC);
        updateComC.setBounds(55,170,90,18);
        panelComenzi.add(updateComC);
        deleteComC.setBounds(110,140,90,18);
        panelComenzi.add(deleteComC);

        displayData.setBounds(40,220,120,25);
        panelComenzi.add(displayData);

        idComanda2.setBounds(10,280,100,20);
        panelComenzi.add(idComanda2);
        idComandaText2.setBounds(100,280,100,20);
        panelComenzi.add(idComandaText2);
        idProdusL.setBounds(10,300,100,20);
        idProdusLText.setBounds(100,300,100,20);
        panelComenzi.add(idProdusL);
        panelComenzi.add(idProdusLText);
        cantitate.setBounds(10,320,100,20);
        panelComenzi.add(cantitate);
        cantitateText.setBounds(100,320,100,20);
        panelComenzi.add(cantitateText);
        total.setBounds(10,340,100,20);
        panelComenzi.add(total);
        totalText.setBounds(100,340,100,20);
        panelComenzi.add(totalText);

        insertComP.setBounds(10,390,80,18);
        panelComenzi.add(insertComP);
        updateComP.setBounds(55,420,90,18);
        panelComenzi.add(updateComP);
        deleteComP.setBounds(110,390,90,18);
        panelComenzi.add(deleteComP);
    }

    public  void setTable(JTable table,int i){
        this.table=table;
        scrtable.setViewportView(table);
        if(i==0) {
            panelClienti.add(scrtable);
            scrtable.setBounds(20,200,700,450);
        }
        if(i==1){
            panelProduse.add(scrtable);
            scrtable.setBounds(20,200,700,450);
        }
        if(i==2){
            panelComenzi.add(scrtable);
            scrtable.setBounds(250,20,430,200);
        }
    }

    public void setTable2(JTable table2){
        scrtable2.setViewportView(table2);
        scrtable2.setBounds(250,270,430,200);
        panelComenzi.add(scrtable2);
    }

    public void setTableComenzi(JTable table5){
        scrtable5.setViewportView(table5);
        scrtable5.setBounds(20,340,750,100);
        frameClient.getContentPane().add(scrtable5);
    }

    public void setTableProduse(JTable table4){
        scrtable4.setViewportView(table4);
        scrtable4.setBounds(20,180,750,150);
        frameClient.getContentPane().add(scrtable4);
    }

    public void openClientFrame(JTable table3, JTable table4, JTable table5){
        frameClient.setTitle("Client");
        frameClient.setSize(800, 600);
        frameClient.setLocation(350, 100);
        frameClient.setLayout(null);
        scrtable3.setViewportView(table3);
        scrtable3.setBounds(20,20,750,150);
        scrtable4.setViewportView(table4);
        scrtable4.setBounds(20,180,750,150);
        scrtable5.setViewportView(table5);
        scrtable5.setBounds(20,340,750,100);
        frameClient.getContentPane().add(scrtable3);
        frameClient.getContentPane().add(scrtable4);
        frameClient.getContentPane().add(scrtable5);
        comandaIdClient.setBounds(20,450,100,20);
        frameClient.getContentPane().add(comandaIdClient);
        comandaIdClientText.setBounds(100,450,100,20);
        frameClient.getContentPane().add(comandaIdClientText);
        comandaIdProduse.setBounds(20,480,150,20);
        comandaIdProduseText.setBounds(150,480,100,20);
        frameClient.getContentPane().add(comandaIdProduse);
        frameClient.getContentPane().add(comandaIdProduseText);
        comandaNrProduse.setBounds(20,510,150,20);
        comandaNrProduseText.setBounds(150,510,100,20);
        frameClient.getContentPane().add(comandaNrProduse);
        frameClient.getContentPane().add(comandaNrProduseText);
        comandaIdComanda.setBounds(300,450,100,20);
        comandaIdComandaText.setBounds(400,450,100,20);
        frameClient.getContentPane().add(comandaIdComanda);

        frameClient.getContentPane().add(comandaIdComandaText);
        comandaMetoda.setBounds(300,480,100,20);
        comandaMetodaText.setBounds(400,480,100,20);
        frameClient.getContentPane().add(comandaMetoda);
        frameClient.getContentPane().add(comandaMetodaText);
        comandaData.setBounds(300,510,100,20);
        comandaDataText.setBounds(400,510,100,20);
        frameClient.getContentPane().add(comandaData);
        frameClient.getContentPane().add(comandaDataText);
        printComanda.setBounds(550,480,200,20);
        frameClient.getContentPane().add(printComanda);


       // frameClient.getContentPane().add(panelClienti);
        frameClient.setVisible(true);
    }
    public void adminButtonListener(ActionListener ev){
        admin.addActionListener(ev);
    }
    public void clientAdminButtonListener(ActionListener ev){
        clientAdmin.addActionListener(ev);
    }

    public void displayPAdminButtonListener(ActionListener ev){displayP.addActionListener(ev);}
    public void insertPButtonListener(ActionListener ev){insertP.addActionListener(ev);}
    public void deletePButtonListener(ActionListener ev){deleteP.addActionListener(ev);}
    public void updatePButtonListener(ActionListener ev){updateP.addActionListener(ev);}
    public void displayCPAdminButtonListener(ActionListener ev){displayC.addActionListener(ev);}
    public void insertCButtonListener(ActionListener ev){insertC.addActionListener(ev);}
    public void deleteCButtonListener(ActionListener ev){deleteC.addActionListener(ev);}
    public void updateCButtonListener(ActionListener ev){updateC.addActionListener(ev);}
    public void displayDataAdminButtonListener(ActionListener ev){displayData.addActionListener(ev);}
    public void insertComCButtonListener(ActionListener ev){insertComC.addActionListener(ev);}
    public void deleteComCButtonListener(ActionListener ev){deleteComC.addActionListener(ev);}
    public void updateComCButtonListener(ActionListener ev){updateComC.addActionListener(ev);}
    public void insertComPButtonListener(ActionListener ev){insertComP.addActionListener(ev);}
    public void deleteComPButtonListener(ActionListener ev){deleteComP.addActionListener(ev);}
    public void updateComPButtonListener(ActionListener ev){updateComP.addActionListener(ev);}
    public void printComandaButtonListener(ActionListener ev){printComanda.addActionListener(ev);}

    public String getIdProdusText(){
        return idProdusText.getText();
    }

    public String getNumePText(){
        return numePText.getText();
    }

    public String getPretPText(){
        return pretPText.getText();

    }

    public String getStocPText(){
        return stocPText.getText();
    }

    public String getIdClientiText(){
        return idClientText.getText();
    }
    public String getNumeCText(){
        return numeCText.getText();
    }
    public String getPrenumeCText(){
        return prenumeCText.getText();
    }
    public String getAdresaCText(){
        return adresaCText.getText();
    }
    public String getEmailCText(){
        return emailCText.getText();
    }
    public String getTelefonCText(){
        return telefonCText.getText();
    }

    public String getIdComandaText(){
        return idComandaText.getText();
    }

    public String getIdComanda2Text(){
        return idComandaText2.getText();
    }

    public String getIdClientLText(){
        return idClientLText.getText();
    }

    public String getMetodaPlataTexr(){
        return metodaPlataText.getText();
    }

    public String getDataComandaText(){
        return dataComandaText.getText();
    }

    public String getTotalPlataText(){
        return totalPlataText.getText();
    }

    public String getIdProdusLText(){
        return idProdusLText.getText();
    }

    public String getCantitateText(){
        return cantitateText.getText();
    }

    public String getTotalText(){
        return totalText.getText();
    }

    public String getComandaIdClientText() {
        return comandaIdClientText.getText();
    }

    public String getComandaIdComandaText() {
        return comandaIdComandaText.getText();
    }

    public String getComandaIdProduseText() {
        return comandaIdProduseText.getText();
    }

    public String getComandaNrProduseText() {
        return comandaNrProduseText.getText();
    }

    public String getComandaMetodaText() {
        return comandaMetodaText.getText();
    }

    public String getComandaDataText() {
        return comandaDataText.getText();
    }
}
