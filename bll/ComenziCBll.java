
package bll;
/**
 * Clasa ComenziCBll este clasa de logica ce implementeaza operatiile de adaugare,stergere,editare a comenzilor C
 * @author      Ripas Raluca
 */

import java.util.List;
import Model.ComenziC;
import dao.ComenziCDAO;
import javax.swing.*;

public class ComenziCBll {

    private List<ComenziC> comenzi;
    ComenziCDAO coD=new ComenziCDAO();
    public ComenziCBll() {
        comenzi=coD.findAll();
    }

    /**
     * Cauta o comandaC dupa id
     * @param id1 id-ul comenziiC cautate
     * @return Returneaza comandaC daca este gasita, altfel returneaza null
     */
    public ComenziC findComandaCById(int id1) {
        ComenziC c = coD.findById(id1);
        if (c == null) {
            System.out.println("Comanda cu id-ul " + id1 + " nu exista!");
        }
        return c;
    }

    /**
     * Insereaza o comandaC in baza  de date
     * @param c comandaC ce trebuie inserata
     */
    public void insertComandaC(ComenziC c) {

        coD.insert(c);
    }

    /**
     * Sterge o comandaC din baza de date
     * @param c comandaC ce trebuie stearsa
     */
    public void deleteComandaC(ComenziC c){
        coD.delete(c.getIdComanda());
    }
    /**
     * Gaseste toate comenzileC
     * @return Returneaza o lista de comenziC
     */
    public List<ComenziC> findAllComenziC(){
        return coD.findAll();
    }
    /**
     * Editeaza o comandaC din baza de date
     * @param p comandaC ce trebuie editata
     */
    public void updateComandaC(ComenziC p){
        coD.update(p);
    }

    /**
     * Insereaza comenzileC intr-un tabel
     * @return Returneaza un tabel populat cu toate comenzileC din baza de date
     */
    public JTable getTable(){
        comenzi=coD.findAll();
        return coD.createTable(comenzi);
    }
    public List<ComenziC> getList(){
        return comenzi;
    }

}
