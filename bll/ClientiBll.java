
package bll;
/**
 * Clasa ClientiBll este clasa de logica ce implementeaza operatiile de adaugare,stergere,editare a clientilor
 * @author      Ripas Raluca
 */
import java.util.List;
import java.util.NoSuchElementException;

import Model.Clienti;
import dao.ClientiDAO;

import javax.swing.*;

public class ClientiBll {

    private List<Clienti> clienti;
    private ClientiDAO clD=new ClientiDAO();
    public ClientiBll() {
        clienti=clD.findAll();
    }

    /**
     * Cauta un client dupa id
     * @param id id-ul clientului cautat
     * @return Returneaza clientul daca este gasit, altfel returneaza null
     */
    public Clienti findClientById(int id) {
        Clienti c = clD.findById(id);
        if (c == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return c;
    }

    /**
     * Insereaza un client in baza  de date
     * @param c clientul ce trebuie inserat
     */
    public void insertClient(Clienti c) {

        clD.insert(c);
    }

    /**
     * Sterge un client din baza de date
     * @param c clientul ce trebuie sters
     */
    public void deleteClient(Clienti c){
        clD.delete(c.getIdClient());
    }

    /**
     * Gaseste toti clientii
     * @return Returneaza o lista de clienti
     */
    public List<Clienti> findAllClienti(){
        return clD.findAll();
    }

    /**
     * Editeaza un client din baza de date
     * @param c clientul ce trebuie editat
     */
    public void updateClient(Clienti c){
        clD.update(c);
    }
    /**
     * Insereaza clientii intr-un tabel
     * @return Returneaza un tabel populat cu toti clientii din baza de date
     */
    public JTable getTable(){
        clienti=clD.findAll();
        return clD.createTable(clienti);
    }
    public List<Clienti> getList(){
        return clienti;
    }

}
