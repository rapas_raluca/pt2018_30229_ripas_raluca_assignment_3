
package bll;
/**
 * Clasa ProduseBll este clasa de logica ce implementeaza operatiile de adaugare,stergere,editare a produselor
 * @author      Ripas Raluca
 */
        import java.util.List;
        import java.util.NoSuchElementException;
        import Model.Produse;
        import dao.ProduseDAO;
        import javax.swing.*;

public class ProduseBll {

    private List<Produse> produse;
    ProduseDAO prD=new ProduseDAO();
    public ProduseBll() {
        produse=prD.findAll();
    }
    /**
     * Cauta un produs dupa id
     * @param id id-ul produsului cautat
     * @return Returneaza produsul daca este gasit, altfel returneaza null
     */
    public Produse findProdusById(int id) {
        Produse p = prD.findById(id);
        if (p == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return p;
    }
    /**
     * Insereaza un produs in baza  de date
     * @param p produsul ce trebuie inserat
     */
    public void insertProdus(Produse p) {

        prD.insert(p);
    }
    /**
     * Sterge un produs din baza de date
     * @param p produsul ce trebuie sters
     */
    public void deleteProdus(Produse p){
        prD.delete(p.getIdProdus());
    }
    /**
     * Gaseste toate produsele
     * @return Returneaza o lista de produse
     */
    public List<Produse> findAllProduse(){
        return prD.findAll();
    }
    /**
     * Editeaza un produs din baza de date
     * @param p produsul ce trebuie editat
     */
    public void updateProdus(Produse p){
        prD.update(p);
    }
    /**
     * Insereaza produsele intr-un tabel
     * @return Returneaza un tabel populat cu toate produsele din baza de date
     */
    public JTable getTable(){
        produse=prD.findAll();
        return prD.createTable(produse);
    }
    public List<Produse> getList(){
        return produse;
    }

}
