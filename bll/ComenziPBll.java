
package bll;
/**
 * Clasa ComenziPBll este clasa de logica ce implementeaza operatiile de adaugare,stergere,editare a comenzilor P
 * @author      Ripas Raluca
 */

import java.util.List;
import java.util.NoSuchElementException;
import Model.ComenziP;
import dao.ComenziPDAO;
import javax.swing.*;

public class ComenziPBll {

    private List<ComenziP> comenzi;
    ComenziPDAO coD=new ComenziPDAO();
    public ComenziPBll() {
        comenzi=coD.findAll();
    }

    /**
     * Cauta o comandaP dupa cele doua id-uri
     * @param id1 id-ul comenzii
     * @param id2 id-ul produsului
     * @return Returneaza comandaP daca este gasita, altfel returneaza null
     */
    public ComenziP findComandaPById(int id1,int id2) {
        ComenziP c = coD.findById(id1,id2);
        if (c == null) {
            throw new NoSuchElementException("The student with id1 =" + id1 + "and id2 ="+id2+" was not found!");
        }
        return c;
    }
    /**
     * Insereaza o comandaP in baza  de date
     * @param c comandaP ce trebuie inserata
     */
    public void insertComandaP(ComenziP c) {

         coD.insert(c);
    }

    /**
     * Sterge o comandaP din baza de date
     * @param c comandaP ce trebuie stearsa
     */
    public void deleteComandaP(ComenziP c){
        coD.delete(c.getIdComanda(),c.getIdProdus());
    }
    /**
     * Gaseste toate comenzileP
     * @return Returneaza o lista de comenziP
     */
    public List<ComenziP> findAllComenziP(){
        return coD.findAll();
    }

    /**
     * Editeaza o comandaP din baza de date
     * @param p comandaP ce trebuie editata
     */
    public void updateComandaP(ComenziP p){
        coD.update(p);
    }
    /**
     * Insereaza comenzileP intr-un tabel
     * @return Returneaza un tabel populat cu toate comenzileP din baza de date
     */
    public JTable getTable(){
        comenzi=coD.findAll();
        return coD.createTable(comenzi);
    }
    //public void update(int id)
    public List<ComenziP> getList(){
        return comenzi;
    }

}
