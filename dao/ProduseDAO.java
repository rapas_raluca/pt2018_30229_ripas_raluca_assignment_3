package dao;
/**
 * Clasa ProduseDAO este un AbstractDAO
 * @author      Ripas Raluca
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
import Model.Produse;
public class ProduseDAO extends AbstractDAO<Produse> {
    protected static final Logger LOGGER = Logger.getLogger(ProduseDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO produse (idProdus,nume,pret,stoc)"
            + " VALUES (?,?,?,?)";
    private static final String updateStatementString = "UPDATE produse SET nume=?,pret=?,stoc=? WHERE idProdus=?";
    private static final String deleteStatementString = "DELETE FROM produse WHERE idProdus=?";
    private final static String findStatementString = "SELECT * FROM produse where idProdus = ?";
    private final static String findAllStatementString= "SELECT * FROM produse";

}
