package dao;
/**
 * Clasa ComenziCDAO este un AbstractDAO
 * @author      Ripas Raluca
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
import Model.ComenziC;
public class ComenziCDAO extends AbstractDAO<ComenziC> {
    protected static final Logger LOGGER = Logger.getLogger(ProduseDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO comenzic (idComanda,idClient,metoda_plata,data_comanda,total_plata)"
            + " VALUES (?,?,?,?,?)";
    private static final String updateStatementString = "UPDATE comenzic SET idClient=?,metoda_plata=?,data_comanda=?,total_plata=? WHERE idComanda=?";
    private static final String deleteStatementString = "DELETE FROM comenzic WHERE idComanda=?";
    private final static String findStatementString = "SELECT * FROM comenzic where idComanda = ?";

}
