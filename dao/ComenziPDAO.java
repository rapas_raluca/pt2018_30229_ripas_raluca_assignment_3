package dao;
/**
 * Clasa ComenziPDAO este un Abstract2DAO
 * @author      Ripas Raluca
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
import Model.ComenziP;
public class ComenziPDAO extends Abstract2DAO<ComenziP> {
    protected static final Logger LOGGER = Logger.getLogger(ProduseDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO comenzip (idComanda,idProdus,cantitate)"
            + " VALUES (?,?,?)";
    private static final String updateStatementString = "UPDATE comenzip SET cantitate=? WHERE idComanda=? AND idProdus=?";
    private static final String deleteStatementString = "DELETE FROM comenzip WHERE idComanda=? and idProdus=?";
    private final static String findStatementString = "SELECT * FROM comenzip where idComanda =? and idProdus=?";

}
