package dao;

/**
 * Clasa Abstract2DAO , clasa abstracta pentru operatiile de adaugare,stergere,editare a unui tabel ce are primary key format din doua id-uri
 * @author      Ripas Raluca
 */
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Connection.ConnectionFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Abstract2DAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public Abstract2DAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    /**
     * Creeaza o interogare de tip SELECT
     * @param field1 numele primului id
     * @param field2 numele celui de-al doilea id
     * @return Returneaza un String cu interogarea
     */
    private String createSelectQuery(String field1,String field2) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM "+type.getSimpleName());
        sb.append(" WHERE " + field1 + " =? AND "+field2+" =?" );
        return sb.toString();
    }
    /**
     * Creeaza o interogare de tip SELECT *
     * @return Returneaza un String cu interogarea
     */
    private String createFindAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM "+type.getSimpleName());
        return sb.toString();
    }
    /**
     * Creeaza o interogare de tip DELETE
     * @param field1 numele primului id
     * @param field2 numele celui de-al doilea id
     * @return Returneaza un String cu interogarea
     */
    private String createDeleteQuery(String field1,String field2) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM "+type.getSimpleName());
        sb.append(" WHERE " + field1 + " =? AND "+field2+" =?");
        return sb.toString();
    }

    /**
     * Creeaza o interogare de tip INSERT
     * @return Returneaza un String cu interogarea
     */
    private String createInsertQuery() {
        int numberOfFields=type.getDeclaredFields().length;
        String s="";
        for(int i=1;i<=numberOfFields;i++){
            if(i!=numberOfFields)
                s=s+"?,";
            else
                s=s+"?";
        }
        String n="(";
        Field[] field= type.getDeclaredFields();
        for (int i=0;i<numberOfFields;i++) {
            if(i!=numberOfFields-1)
                n=n+field[i].getName()+",";
            else
                n=n+field[i].getName();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO "+type.getSimpleName()+" "+n+")");
        sb.append(" VALUES (" + s+")");
        return sb.toString();
    }
    /**
     * Creeaza o interogare de tip UPDATE
     * @param field1 numele primului id
     * @param field2 numele celui de-al doilea id
     * @return Returneaza un String cu interogarea
     */
    private String createUpdateQuery(String field1,String field2) {
        StringBuilder sb = new StringBuilder();
        String s="";
        int numberOfFields=type.getDeclaredFields().length;
        Field[] fields= type.getDeclaredFields();
        for (int i=2;i<numberOfFields;i++) {
            if(i!=numberOfFields-1)
                s=s+fields[i].getName()+"=?,";
            else
                s=s+fields[i].getName()+"=?";
        }
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET "+s);
        sb.append(" WHERE " + field1 + " =? AND "+field2+" =?");
        return sb.toString();
    }

    /**
     * Gaseste toate elementele de tip T in baza de date executand interogarea SELECT*
     * @return Returneaza o lista cu elementele de tip T din baza de date
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createFindAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }


    /**
     * Cauta un element de tip T in baza de date executand interogarea SELECT
     * @param id1 primul id dupa care se sterge
     * @param id2 al doilea id dupa care se sterge
     * @return Returneaza elementul T daca este gasit, altfel returneaza null
     */
    public T findById(int id1,int id2) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Field[] f=type.getDeclaredFields();
        String idS1=f[0].getName();
        String idS2=f[1].getName();
        String query = createSelectQuery(idS1,idS2);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id1);
            statement.setInt(2,id2);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creeaza o lista de elemente de tip T
     * @param resultSet setul de rezultate din urma unei interogari
     * @return Returneaza o lista de elemente de tip T
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Sterge un element de tip T din baza de date executand interogarea DELETE
     * @param id1 primul id dupa care se sterge
     * @param id2 al doilea id dupa care se sterge
     * @return Returneaza elementul T ce a fost sters
     */
    public T delete(int id1,int id2) {
        Connection connection = null;
        PreparedStatement statement = null;
        Field[] f=type.getDeclaredFields();
        String idS1=f[0].getName();
        String idS2=f[1].getName();
        String query = createDeleteQuery(idS1,idS2);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id1);
            statement.setInt(2, id2);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Insereaza un element de tip T in baza de date executand interogarea INSERT
     * @param t elementul T ce va fi inserat
     * @return Returneaza elementul T ce a fost inserat
     */
    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery();
        int nr=type.getDeclaredFields().length;
        Field[] field=type.getDeclaredFields();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            for(int i=0;i<nr;i++) {
                field[i].setAccessible(true);
                statement.setObject(i+1, field[i].get(t));
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Editeaza un element de tip T in baza de date executand interogarea UPDATE
     * @param t elementul T ce  va  fi editat
     * @return Returneaza elementul T ce a fost editat
     */
    public T update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        int nr=type.getDeclaredFields().length;
        Field[] field=type.getDeclaredFields();
        String query = createUpdateQuery(field[0].getName(),field[1].getName());
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int j=1;
            for(int i=2;i<nr;i++) {
                field[i].setAccessible(true);
                statement.setObject(j, field[i].get(t));
                j++;
            }
            field[0].setAccessible(true);
            field[1].setAccessible(true);
            statement.setObject(nr-1,field[0].get(t));
            statement.setObject(nr,field[1].get(t));
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creeaza un tabel dintr-o lista de obiecte
     * @param obj o lista de obiecte oarecare
     * @return Returneaza un tabel populat cu obiecte de tip T
     */
    public JTable createTable(List<T> obj) {
        Field[] field = type.getDeclaredFields();
        int nr = field.length;
        String[] columnName = new String[nr];
        for (int i = 0; i < nr; i++) {
            field[i].setAccessible(true);
            columnName[i] = field[i].getName();
        }

        DefaultTableModel model = new DefaultTableModel(columnName, 0);
        for (T l : obj) {
            Object[] data = new Object[nr];
            try {
                for (int i = 0; i < nr; i++) {
                    field[i].setAccessible(true);
                    data[i] = field[i].get(l);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            model.addRow(data);
        }
        JTable table = new JTable(model);
        return table;
    }
}