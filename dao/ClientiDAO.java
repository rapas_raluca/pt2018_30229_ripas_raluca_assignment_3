package dao;
/**
 * Clasa ClientiDAO este un AbstractDAO
 * @author      Ripas Raluca
 */

import java.util.logging.Logger;
import Model.Clienti;
public class ClientiDAO extends AbstractDAO<Clienti> {
    protected static final Logger LOGGER = Logger.getLogger(ClientiDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO clienti (idClient,nume,prenume,adresa,telefon,email)"
            + " VALUES (?,?,?,?,?,?)";
    private static final String updateStatementString = "UPDATE clienti SET nume=?,prenume=?,adresa=?,telefon=?,email=? WHERE idClient=?";
    private static final String deleteStatementString = "DELETE FROM clienti WHERE idClient=?";
    private final static String findStatementString = "SELECT * FROM clienti where idClient = ?";


    public Clienti findClientById(int id){
        return super.findById(id);
    }

}
