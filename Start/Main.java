package Start;

import Model.Clienti;
import Model.ComenziC;
import Model.ComenziP;
import Model.Produse;
import Presentation.Controller;
import Presentation.View;
import bll.ClientiBll;
import bll.ComenziCBll;
import bll.ComenziPBll;
import bll.ProduseBll;
import dao.ComenziCDAO;
import dao.ComenziPDAO;
import dao.ProduseDAO;
import dao.ClientiDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.List;

public class Main {

    public static void main(String[] arg){

       // Produse pr=new Produse(4,"capsuni",3.4,15);
        //ProduseDAO p= new ProduseDAO();
        ProduseBll bll=new ProduseBll();
        System.out.println(bll.findProdusById(2).toString());
      //  bll.deleteProdus(pr);
       // bll.insertProdus(pr);
       // pr.setNume("ridichi");
       // bll.updateProdus(pr);
       List<Produse> lista=bll.findAllProduse();
      for(Produse l: lista){
        System.out.println(l.toString());
      }

        Clienti c1=new Clienti(7,"Trifan","Ioana","str. Dornei nr.4","0755842918","gergi@yahoo.com");
        ClientiDAO c=new ClientiDAO();
        System.out.println(c.findClientById(1));


       // c.insert(c1);
       // c.delete(c1.getIdClient());
        c1.setAdresa("str. Dornei nr.4");
        c.update(c1);
        List<Clienti> list=c.findAll();
        for(Clienti l:list){
            System.out.println(l.toString());
        }
        String startDate="12-31-2014";
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date date = null;
        try {
            date = sdf1.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
        ComenziC com1=new ComenziC(1,7,sqlStartDate,"numerar",34);
        ComenziP com2=new ComenziP(1,4,10,34);

        ComenziCDAO cdao=new ComenziCDAO();
        ComenziPDAO pdao= new ComenziPDAO();

       // cdao.insert(com1);
      // pdao.insert(com2);

      //  com1.setMetodaPlata("numerar");
      //  cdao.update(com1);
        List<ComenziC> col1=cdao.findAll();
        List<ComenziP> col2=pdao.findAll();

        for(ComenziC cl: col1){
            System.out.println(cl.toString());
        }

        for(ComenziP pl: col2){
            System.out.println(pl.toString());
        }

       // System.out.println(cdao.findById(1,7));

        View v= new View();
        ProduseBll plogic=new ProduseBll();
        ClientiBll clogic=new ClientiBll();
        ComenziPBll cplogic= new ComenziPBll();
        ComenziCBll cclogic= new ComenziCBll();
        Controller controller=new Controller(v,plogic,clogic,cclogic,cplogic);
    }
}
